##########################################################
##
##
## buildStaticFeatures
##
## description: build static design matrix for TTE
##
## created by : shirley.xu@aktana.com
## updated by : wendong.zhu@aktana.com
##              
## updated on : 2019-04-04
##
## Copyright AKTANA (c) 2019.
##
##
##########################################################
library(sparkLearning)
library(sparklyr)
library(dplyr)
library(openssl)
VAL_NO_DATA = 999.8
buildStaticFeatures <- function(prods, accountProduct) {
  
  if(length(prods)==0) { 
    prods <- "accountId"
  } else { 
    prods <- c("accountId",prods)
  }
  
  flog.info("Analyzing account product data")

  # prepare the accountProduct table and build design
  AP <- accountProduct
  AP <- AP %>% group_by(accountId) %>% mutate(temp=row_number(productName)) %>% ungroup() %>% filter(temp==1) %>% select(-temp)
  AP <- AP %>% select(one_of(c(prods)))
  
  if (sdf_ncol(AP) == 1) {
      # static matrix contains only accountId
      return (AP)
  }

  # change NA in string type column to "NA" so it will be counted as distinct count
  colTypes <- sdf_schema(AP)
  colTypes <- sapply(colTypes, function(x){x$type})
  chrV <- names(colTypes)[colTypes=="StringType"]
  
  AP <- AP %>% mutate_at(.vars=chrV, list(~ifelse(is.na(.),'NA',.)))
  AP <- AP %>% mutate_at(.vars=chrV, list(~ifelse(.=='','NA',.)))
  # AP <- AP %>% mutate_at(.vars=chrV, funs(replace(., .=="", 'NA')))
  # pick out variable withs more than 1 unique value (also drop productName column here)
  # AP <- sparkFilterOutColSameValue(AP)

  # colTypes <- sdf_schema(AP)                                   # find the classes of those variables
  # colTypes <- sapply(colTypes, function(x){x$type})
  numV <- names(colTypes)[colTypes=="IntegerType"|colTypes=="DoubleType"] # pick out the non-character (numeric) variables
  AP <- AP %>% mutate_at(.vars=numV, list(~ifelse(is.na(.),as.double(VAL_NO_DATA),.)))
  # AP <- AP %>% mutate_at(.vars=numV, funs(replace(., .=="", VAL_NO_DATA)))
  # chrV <- names(colTypes)[colTypes=="StringType"]         # pick out the character variables
  # for(i in chrV)                                          # for the numeric variables bucket them based on quantiles
  # {                                                             # only do this if there are more than 5 unique values
  #   eval(parse(text=sprintf("AP[is.na(%s),%s:='NA']",i,i)))   #commented because conversion of NA to "NA" is automatically done when copying data frames to Spark
  # }
  # processedDataList <- list()

  # for(i in numV)                                      # for the numeric variables bucket them based on quantiles
  # {                                                           # only do this if there are more than 5 unique values
  #   if(i=="accountId") next                                   # skip if it's the accountId
   
  #   n <- sdf_dim(AP %>% select(i) %>% distinct())[1]
  #   if(n>2)                                                   # only do this if there are more than 5 unique values
  #   {                                                         # could add logic here to make the cut finer - ie more buckets than just the quartiles
  #     colData <- eval(parse(text=sprintf("pull(AP,%s)",i)))
      
  #     # calculate quantiles. unique() is required here.
  #     q <- quantile(unique(colData), probs=seq(0, 1, ifelse(n>5, .25, .5)), na.rm=T)   

  #     if(sum(q>0) < 2) { 
  #       eval(parse(text=sprintf("AP <- select(AP, -%s)", i))) 
  #     } else {
  #       processedColData <- gsub("\\.","_", as.character(cut(colData,unique(q), include.lowest=T)))
  #       processedColData[is.na(processedColData)] <- "missing"
  #       processedDataList[[i]] <- processedColData }
  #   } else { 
  #       eval(parse(text=sprintf("if(sdf_dim(distinct(filter((select(AP,%s)),!is.na(%s))))[1]<2) { AP <- select(AP, -%s)} ",i,i,i))) 
  #   }
  # }

  # flog.info("prepare to sdf_copy_to processedData")

  # if (length(processedDataList) > 0) {
  #     processedData <- sdf_copy_to(spark_connection(AP),data.frame(processedDataList,stringsAsFactors=FALSE), "processedData", overwrite=FALSE, memory=FALSE, repartition=12)
  #     rm(processedDataList)

  #     AP <- AP %>% select(-one_of(colnames(processedData))) %>% sdf_bind_cols(processedData)
  #     AP <- sdf_persist(AP)
  # }
  
  AP <- sparkFilterOutColSameValue(AP)   # pick out variable withs more than 1 unique value

  #    AP <- data.table(model.matrix(~-1+.,AP))                      # now that everything in the AccountProduct table is char and/or factor build the core design matrix without an intercept
  # tmp <- AP %>% mutate_at(.vars=colnames(AP)[colnames(AP)!="accountId"],funs(as.character(.))) %>%               # convert AP columns types all to character for melt
  #   sparkMelt(id_vars="accountId") %>%
  #   dplyr::mutate(value = ifelse(value=="","missing",value)) %>%      # this section replaces the model.matrix() to avoid the contasts issues
  #   dplyr::mutate(value = ifelse(is.na(value), "NA", value)) %>%      # this make sure it map NA as "NA"
  #   sparkCombineCols(c("variable","value"), sep="_", newColName="newvariable", keepOriCols=FALSE) %>% # construct new variable names
  #   dplyr::mutate(newvalue = as.numeric(1)) 

  # AP <- tmp %>% 
  #   sdf_pivot(accountId~newvariable, fun.aggregate=list(newvalue="sum")) %>%
  #   na.replace(0)                                                # cast - basically a transpose
  
  originalColnames <- copy(tbl_vars(AP))
  originalColnames <- originalColnames[originalColnames!="accountId"]
  
  newColnames <- substr(md5(originalColnames), 1, 7) # hashinng col names to reduce long names
  
  predictorNamesAPColMap <- as.list(originalColnames)
  predictorNamesAPColMap <- setNames(predictorNamesAPColMap, as.vector(newColnames))

  # setnames(AP, old=as.character(predictorNamesAPColMap), new=names(predictorNamesAPColMap))
  new <- names(predictorNamesAPColMap)
  old <- as.character(predictorNamesAPColMap)
  AP <- dplyr::rename_at(AP, vars(old), ~ new) # change AP predictors to new names
  
  # if (removeSpecialCharacterInAP) {
  #   names(AP) <- gsub("\n","",names(AP))                          # remove problematic character in colnames after dcast
  # }                                                            # comment out as this is always done when converting to spark dataframe

  # rm(tmp)
  
  flog.info("return from buildStaticFeatures")
  
  return(list(AP=AP, predictorNamesAPColMap=predictorNamesAPColMap))
}
