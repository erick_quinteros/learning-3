#!/bin/bash
#
# aktana-learning Install script for Aktana Learning Engines.
#
# description: Learning modules are written in R. The install script is used
#              to install the appropriate R packages required for the module
#
# created by : satya.dhanushkodi@aktana.com
# modified by : marc.cohen@aktana.com 2/7/2017
#
# created on : 2015-10-13
#
# Copyright AKTANA (c) 2015.
#
PRG="$0"
MODULE="$1"

if [[ "$1x" = "x" ]] 
then
        echo "You should pass a module to the script: anchor, engagement, messageSequence are examples."
        exit 1
fi

LEARNING_HOME=`dirname "$PRG"`/..
export LEARNING_HOME
BIN_DIR=$LEARNING_HOME/bin
RSCRIPT=installPackages.r
PYSCRIPT=requirements.txt

$BIN_DIR/installModule.sh $1 $RSCRIPT $2 $PYSCRIPT

