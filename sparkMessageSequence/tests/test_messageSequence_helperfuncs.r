context('testing messageSequence.prepareDynamicDesignMatrix defined in messageSequence script in spark MSO module')
print(Sys.time())

# load packages and scripts needed for running tests
library(sparkLearning)
library(data.table)
library(arrow)
source(sprintf('%s/common/unitTest/utilsFuncSpark.r',homedir))
# source script contains the testing function
source(sprintf("%s/sparkMessageSequence/messageSequence.R",homedir))

test_that("test messageSequence.prepareDynamicDesignMatrix", {
  # set required input
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_interactions.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_interactionsP.RData', homedir))
  targetNames <- c('a3RA00000001MtAMAU', 'a3RA0000000e0u5MAA', 'a3RA0000000e0wBMAQ', 'a3RA0000000e486MAA', 'a3RA0000000e47gMAA')
  DAYS_CONSIDER_FOR_PRIORVISIT <- 30
  
  # convert to spark data frame
  sc <- initializeSpark(homedir)
  interactions$date <- as.character(interactions$date)
  interactions <- sdf_copy_to(sc, interactions, "interactions", overwrite=TRUE, memory=TRUE)
  interactions <- interactions %>% mutate(date = to_date(date))
  interactionsP$date <- as.character(interactionsP$date)
  interactionsP <- sdf_copy_to(sc, interactionsP, "interactionsP", overwrite=TRUE, memory=TRUE)
  interactionsP <- interactionsP %>% mutate(date = to_date(date))
  # call funcs
  ints_new <- messageSequence.prepareDynamicDesignMatrix(interactions, interactionsP, targetNames, DAYS_CONSIDER_FOR_PRIORVISIT)
  
  # test cases
  # check the result has correct dimension
  expect_equal(sdf_dim(ints_new), c(482,11))
  # check the result is the same as saved
  load(sprintf('%s/messageSequence/tests/data/from_messageSequence_prepareDynamicDesignMatrix_ints.RData', homedir))
  # convert spark dataframe to data table
  ints_new <- convertSDFToDF(ints_new)
  # ints_new$key <- gsub("___","...",ints_new$key,fixed=TRUE)
  setkey(ints,repId,accountId,date,key,interactionId)
  setkey(ints_new,repId,accountId,date,key,interactionId)
  expect_equal(ints_new, ints)
  
  closeSpark(sc)
})
