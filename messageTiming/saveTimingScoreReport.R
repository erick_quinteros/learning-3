##########################################################
#
#
# aktana- messageSequence estimates estimates Aktana Learning Engines.
#
# description: save messageSequence results
#
# created by : wendong.zhu@aktana.com
#
# created on : 2018-08-01
#
# Copyright AKTANA (c) 2016-2018.
#
#
####################################################################################################################

saveTimingScoreReport <- function(con, con_l, scores, tteParams)
{
    library(data.table)
    library(futile.logger)

    flog.info("entered saveTimingScoreReport function...")    

    segs <- tteParams[["segs"]]

    # remove segs not in AP
    accountProduct <- data.table(dbGetQuery(con, sprintf("SELECT * FROM AccountProduct limit 1")))
    segs <- segs[segs %in% colnames(accountProduct)]

    # remove segs not in AP
    accountProduct <- data.table(dbGetQuery(con, sprintf("SELECT * FROM AccountProduct limit 1")))
    segs <- segs[segs %in% colnames(accountProduct)]

    channelUID <- tteParams[["channelUID"]]
    channels <- tteParams[["channels"]]
    tteAlgorithmName <- tteParams[["tteAlgorithmName"]]

    if (is.null(tteAlgorithmName)) {
        flog.info("tteAlgorithmName is NULL. Check whether the externalId/configUID of the build exists in TimeToEngageAlgorithm. Setting to Manual Run.")
        tteAlgorithmName <- "TTE_Manual_Run"
    }

    scores[, c("date") := NULL]
    setnames(scores, "predict", "probability")
    
    nowDate <- Sys.Date()
    nowTime <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")

    ## calculate segment scores for reporting
    scoresSeg <- data.table()
    
    acctSeg <- accounts[, c("accountId", segs), with=F]
    scores.seg <- merge(scores, acctSeg, by="accountId")
    
    for (seg in segs) {
      scoresTmp <- scores.seg[, .(probability=mean(probability)), by=c(seg, "event", "intervalOrDoW", "learningRunUID", "method")]
      scoresTmp$segment <- seg 
      setnames(scoresTmp, seg, "segmentType")
      scoresSeg <- rbind(scoresTmp, scoresSeg, fill=T)
    }
    
    if (!exists("scoresSeg") || length(scoresSeg)==0 ) {
       scoresSeg <- data.table(learningRunUID="NA", segment="NA", segmentType="NA", event="S", method="NA", probability=0, intervalOrDoW=0)
    }

    scoresSeg$createdAt <- nowTime
    scoresSeg$updatedAt <- nowTime 
    scoresSeg$tteAlgorithmName <- tteAlgorithmName

    scoresSeg[event == "S", channelUID := "SEND"]
    scoresSeg[event == "V", channelUID := "VISIT"]
    scoresSeg$event <- NULL
    
    scoresSeg <- scoresSeg[!is.na(segmentType)]

    if (channelUID == "SEND_CHANNEL") {  # optimize SEND channel
        if (!"VISIT" %in% channels) {
            # listen to SEND channel only, so we only output best time to send email after SEND
            # for other channels, always output best time to send email afer VISIT or EVENT
            flog.info("listen to SEND channel in reporting.")
            scoresSeg <- scoresSeg[channelUID == "SEND"] 
        }
    }
    
    dbGetQuery(con_l, sprintf("DELETE FROM SegmentTimeToEngageReport WHERE tteAlgorithmName = '%s'", tteAlgorithmName))

    FIELDS.seg <- list(learningRunUID="varchar(80)", segment="varchar(40)", segmentType="varchar(80)", channelUID="varchar(20)", 
                       probability="double", intervalOrDoW="int", method="varchar(40)", tteAlgorithmName="varchar(80)", createdAt="datetime", updatedAt="datetime")
    
    flog.info("writing to table SegmentTimeToEngageReport ...")    

    dbGetQuery(con_l, "SET FOREIGN_KEY_CHECKS = 0;")

    tryCatch(dbWriteTable(con_l, name="SegmentTimeToEngageReport", value=as.data.frame(scoresSeg), overwrite=F, append=T, row.names=FALSE, field.types=FIELDS.seg),
             error = function(e) {
               flog.error('Error in writing back to table SegmentTimeToEngageReport!', name='error')
               dbDisconnect(con_l)
               quit(save = "no", status = 67, runLast = FALSE) # user-defined error code 67 for failure of writing back to database table
             }
    )    
    
    # export optimal interval by finding max prob
    setorder(scoresSeg, learningRunUID, segment, segmentType, channelUID, method, -probability)
    scoresSeg.max <- scoresSeg[, .SD[1], by=c("learningRunUID", "segment", "segmentType", "channelUID", "method", "tteAlgorithmName")]
    
    dbGetQuery(con_l, sprintf("DELETE FROM TTEsegmentReport WHERE tteAlgorithmName = '%s'", tteAlgorithmName))

    FIELDS.seg.max <- list(learningRunUID="varchar(80)", segment="varchar(40)", segmentType="varchar(80)", channelUID="varchar(20)", 
                       intervalOrDoW="int", method="varchar(40)", probability="double", tteAlgorithmName="varchar(80)", createdAt="datetime", updatedAt="datetime")
    
    flog.info("writing to table TTEsegmentReport ...")    

    tryCatch(dbWriteTable(con_l, name="TTEsegmentReport", value=as.data.frame(scoresSeg.max), overwrite=F, append=T, row.names=FALSE, field.types=FIELDS.seg.max),
             error = function(e) {
               flog.error('Error in writing back to table TTEsegmentReport!', name='error')
               dbDisconnect(con_l)
               quit(save = "no", status = 67, runLast = FALSE) # user-defined error code 67 for failure of writing back to database table
             }
    )    
    
    ## save scores reporting results    
    scores[event == 'S',  event := '12'] # SEND_ANY
    scores[event == 'V', event := '3']   # VISIT
    scores[event == 'W', event := '13']  # WEB_INTERACTION
    scores[event == 'VS', event := '5']  # VISIT_SAMPLES
    
    scores[, event := as.integer(event)]
    setnames(scores, "event", "repActionTypeId")  # rename event to repActionTypeId

    if (channelUID == "SEND_CHANNEL") {  # optimize SEND channel
        if (!"VISIT" %in% channels) {  
            # listen to SEND channel only, so we only output best time to send email after SEND
            # for other channels, always output best time to send email afer VISIT or EVENT
            flog.info("listen to SEND channel in reporting.")
            scores <- scores[repActionTypeId == 12] 
        }
    }

    scores$runDate <- nowDate
    scores$createdAt <- nowTime
    scores$updatedAt <- nowTime      
    scores$tteAlgorithmName <- tteAlgorithmName

    dbGetQuery(con_l, sprintf("DELETE FROM AccountTimeToEngageReport WHERE tteAlgorithmName = '%s'", tteAlgorithmName))
    
    flog.info("writing to table AccountTimeToEngageReport ...")    

    FIELDS <- list(learningRunUID="varchar(80)", accountId="int(11)", repActionTypeId="tinyint", 
                   intervalOrDoW="int", method="varchar(40)", tteAlgorithmName="varchar(80)", probability="double", runDate="date", createdAt="datetime", updatedAt="datetime")
    
    # append new scores/probs
    startTimer <- Sys.time()
    tryCatch(dbWriteTable(con_l, name="AccountTimeToEngageReport", value=as.data.frame(scores), overwrite=F, append=T, row.names=FALSE, field.types=FIELDS),
             error = function(e) {
               flog.error('Error in writing back to LearningDB table AccountTimeToEngageReport!', name='error')
               dbDisconnect(con)
               quit(save = "no", status = 67, runLast = FALSE) # user-defined error code 67 for failure of writing back to database table
             }
    )
    flog.info("Write new scores to AccountTimeToEngageReport in Learning DB: Time = %s", Sys.time()-startTimer)
    
    setorder(scores, learningRunUID, accountId, repActionTypeId, method, -probability)
    
    scores.max <- scores[, .SD[1], by=c("learningRunUID", "accountId", "repActionTypeId", "method", "tteAlgorithmName")]
    
    # convert repActionTypeId to channelUID
    scores.max[repActionTypeId == 12, channelUID := "SEND"]
    scores.max[repActionTypeId == 3, channelUID := "VISIT"]
    scores.max$repActionTypeId <- NULL
    
    accountIdMap <- data.table(dbGetQuery(con, "SELECT accountId, externalId FROM Account WHERE isDeleted=0;"))
    
    # get accountUID from accountId
    scores.max <- merge(scores.max, accountIdMap, by="accountId")
    setnames(scores.max, "externalId", "accountUID")
    scores.max$accountId  <- NULL
    scores.max$runDate <- NULL

    dbGetQuery(con_l, sprintf("DELETE FROM TTEhcpReport WHERE tteAlgorithmName = '%s'", tteAlgorithmName))

    FIELDS <- list(learningRunUID="varchar(80)", accountUID="varchar(80)", channelUID="varchar(20)",
                   intervalOrDoW="int", probability="double", tteAlgorithmName="varchar(80)", createdAt="datetime", updatedAt="datetime")
    
    flog.info("writing to table TTEhcpReport ...") 
    
    # append new scores/probs
    startTimer <- Sys.time()
    tryCatch(dbWriteTable(con_l, name="TTEhcpReport", value=as.data.frame(scores.max), overwrite=F, append=T, row.names=FALSE, field.types=FIELDS),
             error = function(e) {
               flog.error('Error in writing back to LearningDB table TTEhcpReport!', name='error')
               dbDisconnect(con)
               quit(save = "no", status = 67, runLast = FALSE) # user-defined error code 67 for failure of writing back to database table
             }
    ) 
    flog.info("Append new scores to TTEhcpReport: Time = %s", Sys.time()-startTimer)
    
    dbGetQuery(con_l, "SET FOREIGN_KEY_CHECKS = 1;")
}
