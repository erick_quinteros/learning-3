##########################################################
#
#
# aktana-learning Install Aktana Learning Engines.
#
# description: read the DB and save in list in .RData file
#
#
# created by : marc.cohen@aktana.com
#
# created on : 2015-10-13
#
# Copyright AKTANA (c) 2015.
#
#
##########################################################

readV3db <- function()
    {
        library(RMySQL)
        library(data.table)

        flog.info("saveData %s %s %s %s",dbuser,dbpassword,dbhost,dbname)

        drv <- dbDriver("MySQL")
        con <- switch((!exists("port"))+1, dbConnect(drv,user=dbuser,password=dbpassword,host=dbhost,dbname=dbname,port=port), dbConnect(drv,user=dbuser,password=dbpassword,host=dbhost,dbname=dbname))

        dbGetQuery(con,"SET NAMES utf8")

        flog.info("read accounts")
        accounts <- data.table(dbGetQuery(con,"SELECT * FROM Account WHERE isDeleted=0;"))

        flog.info("read reps")
        reps <- data.table(dbGetQuery(con,"SELECT r.repId, r.repName, rtr.repTeamName FROM Rep as r JOIN RepTeamRep using (repId) JOIN RepTeam rtr using (repTeamId) WHERE isActivated=1;"))

        flog.info("read repAssignments")
        repAssignments <- data.table(dbGetQuery(con,"SELECT repId, accountId FROM RepAccountAssignment;"))

        accts <- unique(repAssignments[repId %in% reps$repId]$accountId)

        flog.info("read facilities")
        facilities <- data.table(dbGetQuery(con,"SELECT facilityId, externalId, facilityName, geoLocationString, latitude, longitude FROM Facility;"))

        flog.info("read accountProduct")
        accountProduct <- data.table(dbGetQuery(con,"SELECT ap.*, p.productName FROM AccountProduct as ap JOIN Product p using (productId);"))
        accountProduct$productId <- NULL
        accountProduct <- accountProduct[accountId %in% accts]
        accounts <- accounts[accountId %in% accts]

        flog.info("read targets")
        targetSQL <- "SELECT tc.startDate, tc.endDate, s.repId, s.accountId, s.target, tl.targetingLevelName, ra.repActionTypeName, p.productName FROM .StrategyTarget as s JOIN Product p using (productId) JOIN TargetingLevel tl using (targetingLevelId) JOIN RepActionType ra using (repActionTypeId) JOIN TargetsPeriod tc using (targetsPeriodId);"
        targets <- data.table(dbGetQuery(con,targetSQL))

        flog.info("read interactions")
        interactionsSQL <- "SELECT  v.interactionId, v.externalId, v.repId, v.facilityId, v.wasCreatedFromSuggestion, ia.accountId, vt.interactionTypeName, IFNULL(ipm.messageId, vp.messageId) as messageId, IFNULL(ipm.physicalMessageUID, vp.physicalMessageUID), pi.productInteractionTypeName, p.productName, v.duration, v.startDateLocal FROM Interaction as v JOIN InteractionType vt using (interactionTypeId) JOIN InteractionAccount ia using (interactionId) JOIN InteractionProduct vp using (interactionId) JOIN ProductInteractionType pi using (productInteractionTypeId) JOIN Product p using (productId) LEFT JOIN InteractionProductMessage ipm ON vp.`interactionId` = ipm.`interactionId` and vp.`productId` = ipm.`productId` WHERE v.isCompleted=1 AND v.isDeleted=0;"
        interactions <- data.table(dbGetQuery(con,interactionsSQL))
        setnames(interactions,"IFNULL(ipm.physicalMessageUID, vp.physicalMessageUID)","physicalMessageUID")  # kludge to correct SQL - we can live with it.
        
        flog.info("read events")
        eventsSQL <- "SELECT  e.repId, e.accountId, e.eventDate, et.eventTypeName, e.eventLabel, e.eventDateTimeUTC, e.messageId, e.physicalMessageUID, p.productName FROM .Event as e JOIN .EventType et using (eventTypeId) JOIN .Product p on p.productId=e.productId;"
        events <- data.table(dbGetQuery(con,eventsSQL))
        events <- events[accountId %in% accts]

        products <- data.table(dbGetQuery(con,"SELECT productName, externalId FROM Product WHERE isActive=1 and isDeleted=0;"))

        # DSEconfig <- data.table(dbGetQuery(con,"SELECT seConfigId, seConfigName, description, configSpecjson FROM DSEConfig;"))

        # clean up interactions data
        interactions <- interactions[accountId %in% accts]
        interactions$date <- as.Date(interactions$startDateLocal)
        interactions$startDateLocal <- NULL
        interactions[,c("interactionTypeName","productInteractionTypeName","productName"):=list(as.factor(interactionTypeName),as.factor(productInteractionTypeName),as.factor(productName))]


        flog.info("read messages")
        messages <- data.table(dbGetQuery(con,"Select m.messageId, m.lastPhysicalMessageUID, m.messageTopicId, m.messageName, m.messageDescription, p.productName FROM Message as m JOIN Product p using(productId);"))

        flog.info("read messageSet")
        messageSet <- data.table(dbGetQuery(con,"Select messageId, messageSetId FROM MessageSetMessage;"))

        # approvedDoc <- data.table(dbGetQuery(con,sprintf("select * from %s_cs.%s where IsDeleted=0;","pfizerprod","Approved_Document_vod__c")))
        # cols <- names(approvedDoc)
        # cols <- gsub("_vod__c","",cols)
        # cols <- gsub("__c","",cols)
        # names(approvedDoc) <- cols
        # setnames(approvedDoc,"Id","physicalMessageUID")
        # 
        # approveDoc <- approvedDoc[,c("physicalMessageUID","Name","Document_Description","Email_Subject","Status","Product","Email_HTML_1"),with=F]
                                            
        ## describe <- function(table)
        ##     {
        ##         tmp <- data.table(dbColumnInfo(dbSendQuery(con,sprintf("SELECT * FROM %s.%s;",dbname,table))))
        ##         dbClearResult(dbListResults(con)[[1]])
        ##         tmp <- tmp[grepl("_akt",name)]
        ##         tmp[,c("type","length"):=NULL]
        ##         setnames(tmp,c("name","Sclass"),c("field","type"))
        ##         tmp[,c("table","field","type"):=list(as.character(table),as.character(field),as.character(type))]
        ##         return(tmp)
        ##     }


         #  read the table info for some of the tables
         ## meta1 <- describe('Account')
         ## meta2 <- describe('AccountProduct')
         ## meta2 <- rbind(meta2,data.table(table="AccountProduct",field="productName",type="character"))
         ## meta3 <- describe('Rep')

         ## METADATA <- rbind(meta1,meta2,meta3)

         ## for(i in 1:(dim(METADATA)[1]))
         ##    {
         ##        if(METADATA[i]$table=="Account")tbl <- "accounts"
         ##        else tbl <- "accountProduct"
         ##        eval(parse(text=sprintf("METADATA[i,uniqueValues:=length(unique(%s$%s))]",tbl,METADATA[i]$field)))
         ##        if(METADATA[i]$type!="character")
         ##            {
         ##                eval(parse(text=sprintf("q<-quantile(%s$%s,na.rm=TRUE)",tbl,METADATA[i]$field)))
         ##                eval(parse(text=sprintf("METADATA[i,c('Q0','Q25','Q50','Q75','Q100'):=list(q[1],q[2],q[3],q[4],q[5])]",tbl,METADATA[i]$field)))
         ##            }
         ##         eval(parse(text=sprintf("nas<-dim(%s[is.na(%s)])[1]",tbl,METADATA[i]$field)))
         ##         eval(parse(text=sprintf("METADATA[i,c('numberNAs','percentNAs'):=list(nas,100*nas/dim(%s)[1])]",tbl,tbl)))
         ##    }

         dbDisconnect(con)

        flog.info("return from V3read")
        return(list(accounts=accounts,reps=reps,repAssignments=repAssignments,facilities=facilities,targets=targets,interactions=interactions,messages=messages,messageSet=messageSet,accountProduct=accountProduct,events=events,products=products)) #,DSEconfig=NULL,METADATA=METADATA,approvedDoc=approvedDoc))
    }

