CREATE TABLE `RepAccountEngagement` (
  `repActionTypeId` int(11) DEFAULT NULL,
  `repId` int(11) DEFAULT NULL,
  `accountId` int(11) DEFAULT NULL,
  `suggestionType` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `probability` double DEFAULT NULL,
  `runDate` date DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci